var savegame = JSON.parse(localStorage.getItem("save"));

var glifPrice = 256;
var factor = 1;
var numbers = {
  nb: 0,
  pixNb: 0,
  nbFusion: 0,
  imgNb: 1,
  maxGem: 0,
  factor: 1,
  nbSlot: 0,
  nbGlif: 0,
  upPrice: factor * 8,
  slotPrice: 256,
  imgs: [{
    0: 'no'
  }]
}

var interface = document.getElementsByTagName('main')[0];

var buttons = document.getElementsByClassName('button');
var buttonTheo = document.getElementById('buttonTheo');
var buttonUp = document.getElementById('buttonUpgrade');
var buttonBuySlot = document.getElementById('buttonSlot');
var buttonBuyGlif = document.getElementById('buttonGlif');
var buttonBuyFusion = document.getElementById('buttonFusion');

var infoBox = document.getElementById('infoBox');
var consoleTheo = document.getElementById('console');

var counterUnit = document.getElementById('incrementUnit');
var pixelCounter = document.getElementById('pixelCounter');
var pix = pixelCounter.getElementsByTagName('td');
var log = document.getElementById('console');

var imageContainer = document.getElementById('images');
var boxes = imageContainer.getElementsByTagName('td');
var imgs = imageContainer.getElementsByTagName('img');

var sentences = document.getElementsByClassName('sentences');

if (savegame) {
  factor = savegame.factor
  numbers = savegame;

  // init
  getGlifsOnLoad();
  afterIncrement();
  showSlotOnLoad();
}

buttonBuySlot.addEventListener('click', buySlot);
buttonBuyGlif.addEventListener('click', buyGlif);
buttonUp.addEventListener('click', upgrade);
buttonTheo.addEventListener('click', increment);
for (let i = 0; i < boxes.length; i++) {
  const box = boxes[i];
  box.addEventListener('click', (bx) => {
    setFusion(box);
  })

  for (let i = 0; i < buttons.length; i++) {
    buttonBuyFusion.addEventListener('click', makeFusion);
    const button = buttons[i];
    button.addEventListener('mouseover', (btn) => {
      showPrice(button);
    })
    button.addEventListener('mouseout', hidePrice);

  }
}

setInterval(() => {
  randomSentences();
}, 1000)

setInterval(() => {
  miningPixels();
  localStorage.setItem("save", JSON.stringify(numbers));
}, 1500);

setInterval(() => {
  for (let i = 0; i < boxes.length; i++) {
    var image = boxes[i].getElementsByTagName('img')[0];
    var rand = Math.floor((Math.random() * 4) + 0) * 90;
    image.style.transform = 'rotate(' + rand + 'deg)';
  }
}, 100);

// chut!
cheat();