function increment() {

  numbers.nb += factor;
  numbers.pixNb += factor;

  afterIncrement();
}

function afterIncrement() {
  showConsole(factor, '+', '#00FF00');
  fillPixelCounter();
  showButtons();
  counterUnit.innerHTML = '<li>' + numbers.nb + '</li>';
  checkCredits();
}


function resetpixelCounter(pix) {
  for (let i = 0; i < pix.length; i++) {
    pix[i].removeAttribute('style');
  }
}

function fillPixelCounter() {
  for (let i = 0; i < pix.length; i++) {
    const el = pix[i];
    el.style.outline = '';
    if (i <= numbers.pixNb) {
      el.style.outline = '1px solid';
    }
  }
}

function showConsole(num, operator, color, position) {
  var chrisme = (operator == '+') ? 'chrismeV' : 'chrismeR';
  var style = 'color:' + color + '; text-align:' + position + ';';
  var el = '<div style="' + style + '">' + operator + num + '<img src="img/' + chrisme + '.bmp"/></div>';
  consoleTheo.innerHTML = el;
  setTimeout(() => {
    consoleTheo.innerHTML = '';
  }, 250);
}

function upgrade() {
  if (numbers.nb >= 8 && numbers.nb >= numbers.upPrice) {
    factor = factor + 1;
    numbers.nb -= numbers.upPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.upPrice, '-', '#FF0000');
    fillPixelCounter();
    numbers.upPrice += factor;
    checkCredits();
  }
}

function buySlot() {
  if (numbers.nb >= numbers.slotPrice) {
    numbers.nb -= numbers.slotPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.slotPrice, '-', '#FF0000');
    fillPixelCounter();
    showSlot();

    buttonBuyGlif.style.display = 'block';
    numbers.slotPrice = numbers.slotPrice * 2;
    checkCredits();
  }
}

function buyGlif() {
  if (numbers.nb >= glifPrice && numbers.nbSlot > numbers.nbGlif) {
    numbers.nb -= glifPrice;
    refreshCounter(counterUnit, numbers.nb);
    showConsole(glifPrice, '-', '#FF0000');
    fillPixelCounter();
    showGlif();
    showButtons();
    checkCredits();
  }
}

function refreshCounter(counter, ind) {
  counter.innerHTML = '<li>' + ind + '</li>';
}


function killGlif(image) {
  image.setAttribute('src', 'img/red.bmp');
  var intervalId = null;
  var timer = 50;
  var kill = function() {
    if (timer > 0) {
      var op = Math.floor((Math.random() * 2) + 0);
      image.style.opacity = op;
      timer--;
    } else if (timer == 0) {
      image.style.opacity = 1;
      image.setAttribute('src', 'img/black.bmp');
      image.setAttribute('data-nb', '0')
      updateImgsArray()
      showButtons();
      clearInterval(intervalId);
    }
  };
  intervalId = setInterval(kill, 8);
  numbers.nbGlif--;

}

function setFusion(box) {
  var img = box.children[0];
  var theGlif = parseInt(img.getAttribute('data-nb'));
  if (theGlif > 0) {
    if (img.classList.contains('select')) {
      img.classList.remove('select');
    } else {
      img.classList.add('select');
    }
  }
}

function makeFusion() {
  var select = images.getElementsByClassName('select');
  if (select.length > 1) {
    var nbFusion = 0;
    for (let i = 0; i < select.length; i++) {
      var slc = select[i];
      var theGlif = parseInt(slc.getAttribute('data-nb'));
      nbFusion += theGlif;
      if (i != 0) {
        killGlif(slc);
      }
    }
    select[0].setAttribute('src', 'img/gifs/' + nbFusion + '.gif');
    select[0].setAttribute('data-nb', nbFusion);
    for (let i = 0; i < imgs.length; i++) {
      const img = imgs[i];
      img.classList.remove('select');
    }
    checkCredits()
  }
}

function checkCredits() {
  (numbers.upPrice > numbers.nb) ? buttonUp.classList.add('gray'): buttonUp.classList.remove('gray');
  (numbers.slotPrice > numbers.nb) ? buttonBuySlot.classList.add('gray'): buttonBuySlot.classList.remove('gray');
  if (numbers.nb < glifPrice || numbers.nbSlot <= numbers.nbGlif) {
    buttonBuyGlif.classList.add('gray');
  } else {
    buttonBuyGlif.classList.remove('gray');
  }
}

function showSlot() {
  let image = boxes[numbers.nbSlot].getElementsByTagName('img')[0];
  image.style.outline = '1px solid rgb(82,82,82)';
  numbers.nbSlot++;
}

function showSlotOnLoad() {
  for (let i = 0; i < numbers.nbSlot; i++) {
    let image = boxes[i].getElementsByTagName('img')[0];
    image.style.outline = '1px solid rgb(82,82,82)';
    buttonBuyGlif.style.display = 'block';
  }
}

function hideSlotAndGlif() {
  for (let i = 0; i < boxes.length; i++) {
    let image = boxes[i].getElementsByTagName('img')[0];
    image.style.outline = '0px solid rgb(82,82,82)';
    image.setAttribute('src', 'img/black.bmp');
    image.setAttribute('data-nb', '0')
  }
}

function showGlif() {
  for (let i = 0; i < boxes.length; i++) {
    let image = boxes[i].getElementsByTagName('img')[0];
    let theGlif = parseInt(image.getAttribute('data-nb'));
    if (theGlif == 0) {
      image.setAttribute('src', 'img/gifs/1.gif');
      image.setAttribute('data-nb', '1');
      numbers.nbGlif++;
      numbers.maxGem++;
      updateImgsArray();
      return;
    }
  }
}

function updateImgsArray() {
  for (let i = 0; i < boxes.length; i++) {
    let image = boxes[i].getElementsByTagName('img')[0];
    let dataNb = image.getAttribute('data-nb');
    numbers.imgs[i] = dataNb;
  }
}

function getGlifsOnLoad() {
  if (numbers.imgs[1]) {
    for (let i = 0; i < boxes.length; i++) {
      let image = boxes[i].getElementsByTagName('img')[0];
      let nb = numbers.imgs[i];
      if (nb && nb != '0') {
        image.setAttribute('src', 'img/gifs/' + nb + '.gif');
        image.setAttribute('data-nb', nb);
      }
    }
  }
}

function showButtons() {
  if (numbers.nb >= 8) {
    buttonUp.style.display = 'block';
  }
  if (numbers.nb >= 256) {
    buttonBuySlot.style.display = 'block';
  }
  if (numbers.pixNb >= 256) {
    numbers.pixNb = 0;
  }
  if (numbers.nbGlif >= 2) {
    buttonBuyFusion.style.display = 'block';
  }
  if (numbers.nb <= 1) {
    buttonUp.style.display = 'none';
    buttonBuySlot.style.display = 'none';
    buttonBuyFusion.style.display = 'none';
    buttonBuyGlif.style.display = 'none';
    numbers.pixNb = 0;

  }
}

function showSentences(index) {
  var actualSentences = sentences[index];
  if (index > 0) {
    var prevSentences = sentences[index - 1];
    prevSentences.style.display = 'none';
  }
  actualSentences.style.display = 'block';
}

function randomSentences() {
  for (let i = 0; i < sentences.length; i++) {
    if (sentences[i].style.display == 'block') {
      var sentence = sentences[i].getElementsByTagName('p');
      var rand = Math.floor((Math.random() * sentence.length) + 0);
      for (let j = 0; j < sentence.length; j++) {
        sentence[j].style.display = 'none';
      }
      sentence[rand].style.display = 'inline';
    }
  }
}

function miningPixels() {
  numbers.nb = numbers.nb + numbers.maxGem;
  if (numbers.nbGlif != 0) {
    refreshCounter(counterUnit, numbers.nb);
    showConsole(numbers.maxGem, '+', '#00FF00', 'right');
    fillPixelCounter();
  }
}

function showPrice(btn) {
  let id = btn.id;
  let color = btn.classList[1];
  let message = '';
  let chrisme = (color == 'gray') ? 'chrismeG' : 'chrisme'
  let image = '<img src="img/' + chrisme + '.bmp"/></div>'
  if (id == 'buttonTheo') {
    message = '<span style="color:' + color + ';">+' + factor + image + '</span>';
  } else if (id == 'buttonUpgrade') {
    message = '<span style="color:' + color + ';">-' + numbers.upPrice + image + '</span>';
  } else if (id == 'buttonSlot') {
    message = '<span style="color:' + color + ';">-' + numbers.slotPrice + image + '</span>';
  } else if (id == 'buttonGlif') {
    message = '<span style="color:' + color + ';">-' + glifPrice + image + '</span>';
  }
  infoBox.innerHTML = message;
}

function hidePrice() {
  infoBox.innerHTML = '';
}


function cheat() {
  document.addEventListener('keydown', (event) => {
    const key = event.key;

    if (key == 'Shift') {
      numbers.nb += 1000;
      fillPixelCounter();
      showButtons();
      counterUnit.innerHTML = '<li>' + numbers.nb + '</li>';
      checkCredits();
    } else if (key == 'Escape') {
      factor = 1;
      numbers = {
        nb: 0,
        pixNb: 0,
        nbFusion: 0,
        imgNb: 1,
        maxGem: 0,
        factor: 1,
        nbSlot: 0,
        nbGlif: 0,
        upPrice: factor * 8,
        slotPrice: 256,
        imgs: [{
          0: 'no'
        }]
      }
      console.log(numbers.imgs);
      afterIncrement();
      showSlotOnLoad();
      hideSlotAndGlif();
    }
  })
}