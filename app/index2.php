
  <main>
      
    <input type="button" class="button techno" id="button" value="">
    <input type="button" class="button" id="buttonUpgrade" value="">
    <input type="button" class="button" id="buttonGlif" value="">

    <ul id="counter"><li>0</li></ul>

    <?php 
      echo '<table id="pixelCounter">';
      for ($i=0; $i < 16; $i++) {
        echo '<tr>';
        for ($j=0; $j < 16; $j++) {
          echo '<td></td>';
        }
        echo '</tr>';
      }
      echo '</table>';
    ?>

    <?php      
      echo '<table id="images">';
      for ($i=0; $i < 4; $i++) {
        echo '<tr>';
        for ($j=0; $j < 4; $j++) {
          echo '<td><img src=""></td>';
        }
        echo '</tr>';
      }
      echo '</table>';
    ?>

  </main>
  
  <section id="console"></section>
  
  <section id="texte">
    <?php 
      $contents = getText('lists/words.txt'); 
      foreach ($contents as $key => $content) {
        $nb = $key - 1;
        echo '<div class="sentences" data-nb="'.$nb.'">';
          foreach ($content as $key => $sentence) {
            echo '<p>'.$sentence.'</p>';
          }
        echo '</div>';
      }
    ?>
  </section>

  <section id="infoBox"></section>
