<?php 
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  include_once('inc/functions.php'); 
?>

<!DOCTYPE html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NevrOS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="style/main.css">
  </head>
  <body>

  <section id="infoBox"></section>

  <main id="theo">
       
    <div id="buttons">
      <input type="button" class="button" id="buttonTheo" value="">
      <input type="button" class="button" id="buttonUpgrade" value="">
      <input type="button" class="button" id="buttonSlot" value="">
      <input type="button" class="button" id="buttonGlif" value="">
      <input type="button" class="button" id="buttonFusion" value="">
    </div>

    <ul id="counter">
      <li id="counterBonus"></li>
      <li id="counterUnit">
      <?php 
        echo '<table id="pixelCounter">';
        for ($i=0; $i < 16; $i++) {
          echo '<tr>';
          for ($j=0; $j < 16; $j++) {
            echo '<td></td>';
          }
          echo '</tr>';
        }
        echo '</table>';
      ?>
        <span id="incrementUnit">0</span>
        <img src="img/chrisme.bmp" alt="">
      </li>
    </ul>



    <?php      
      echo '<table id="images">';
      for ($i=0; $i < 4; $i++) {
        echo '<tr>';
        for ($j=0; $j < 4; $j++) {
          echo '<td><img src="img/black.bmp" data-nb="0"></td>';
        }
        echo '</tr>';
      }
      echo '</table>';
    ?>

  </main>

  <section id="console"></section>
  
  <section id="texte">
    <?php 
      $contents = getTextFile('lists/words.txt'); 
      foreach ($contents as $key => $content) {
        $nb = $key - 1;
        echo '<div class="sentences" data-nb="'.$nb.'">';
          foreach ($content as $key => $sentence) {
            echo '<p>'.$sentence.'</p>';
          }
        echo '</div>';
      }
    ?>
  </section>

    <?php //include('index2.php'); ?>

  </body>
  <script src="scripts/functions.js"></script>
  <script src="scripts/main.js"></script>
</html