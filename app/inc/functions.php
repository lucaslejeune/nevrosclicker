<?php

function getTextFile($file) {
  $file = file_get_contents($file);
  $gems = explode('----', $file);
  $gems = array_filter($gems);
  foreach ($gems as $i => $gem) {
    $sentences = explode(PHP_EOL, $gem);
    $sentences = array_filter($sentences);
    $gems[$i] = $sentences;
  }
  return $gems;
}

?>